# How To Approve Testimonials

1. Login to WordPress Dashboard by visiting [http://mikeleachpainting.com.au/wp-admin](http://mikeleachpainting.com.au/wp-admin).
    * Fill Username (1)
    * Fill Password (2)
    * Solve Spam Protection Math Question (3)
    * Click on Log in (4)

![Login](images/leach_login.jpg)

2. Click on Testimonials (1)

![Testimonials](images/leach_testimonials.jpg)

3. Hover on a testimonial and click `Edit` (1).

![Edit](images/leach_edit.jpg)

4. After the testimonial loads, click on `Publish` (1).

![Publish](images/leach_update.jpg)

5. That's it. The testimonial should be published now.